curl -v http://0.0.0.0:3001/certifications
curl -v -X POST http://0.0.0.0:3001/certifications
curl -v -X PUT http://0.0.0.0:3001/certifications
curl -v -X DELETE http://0.0.0.0:3001/certifications

curl -v http://0.0.0.0:3001/education
curl -v -X POST http://0.0.0.0:3001/education
curl -v -X PUT http://0.0.0.0:3001/education
curl -v -X DELETE http://0.0.0.0:3001/education

curl -v http://0.0.0.0:3001/git_repos
curl -v -X POST http://0.0.0.0:3001/git_repos
curl -v -X PUT http://0.0.0.0:3001/git_repos
curl -v -X DELETE http://0.0.0.0:3001/git_repos

curl -v http://0.0.0.0:3001/personal_data
curl -v -X POST http://0.0.0.0:3001/personal_data
curl -v -X PUT http://0.0.0.0:3001/personal_data
curl -v -X DELETE http://0.0.0.0:3001/personal_data

curl -v http://0.0.0.0:3001/portfolio
curl -v -X POST http://0.0.0.0:3001/portfolio
curl -v -X PUT http://0.0.0.0:3001/portfolio
curl -v -X DELETE http://0.0.0.0:3001/portfolio

curl -v http://0.0.0.0:3001/profesional_experience
curl -v -X POST http://0.0.0.0:3001/profesional_experience
curl -v -X PUT http://0.0.0.0:3001/profesional_experience
curl -v -X DELETE http://0.0.0.0:3001/profesional_experience

curl -v http://0.0.0.0:3001/skills
curl -v -X POST http://0.0.0.0:3001/skills
curl -v -X PUT http://0.0.0.0:3001/skills
curl -v -X DELETE http://0.0.0.0:3001/skills
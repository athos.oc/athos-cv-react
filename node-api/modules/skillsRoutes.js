const express = require('express')
const app = express()
const pool = require('./db')

app.get('/', (request, response) => {
    pool.query('SELECT * FROM skills')
    .then(res=>{
        response.status(200).json(res.rows)
    }).catch(err=>{
        response.status(400).json(err)
    })
});

app.post('/', (request, response) => {
    response.status(200).json('skills post World!')
});

app.put('/', (request, response) => {
    response.status(200).json('skills update World!')
});

app.delete('/', (request, response) => {
    response.status(200).json('skills delete World!')
});

module.exports = app
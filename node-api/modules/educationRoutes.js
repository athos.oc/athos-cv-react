const express = require('express')
const app = express()
const pool = require('./db')

app.get('/', (request, response) => {
    pool.query('SELECT * FROM education')
    .then(res=>{
        response.status(200).json(res.rows)
    }).catch(err=>{
        response.status(400).json(err)
    })
});

app.post('/', (request, response) => {
    response.status(200).json('post World!')
});

app.put('/', (request, response) => {
    response.status(200).json('update World!')
});

app.delete('/', (request, response) => {
    response.status(200).json('delete World!')
});

module.exports = app
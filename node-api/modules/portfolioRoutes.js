const express = require('express')
const app = express()
const pool = require('./db')

app.get('/', (request, response) => {

    pool.query('SELECT * FROM portfolio')
    .then(async portfolios=>{
        const aux = []

        for (let i = 0; i < portfolios.rows.length; i++) {
            const portfolio = portfolios.rows[i];
            portfolio.technologies = []
            await pool.query(`SELECT * FROM portfolio_technologies where portfolio_id = ${portfolio.id}`)
                .then(async portfolio_relation_technologies=>{

                    for (let j = 0; j < portfolio_relation_technologies.rows.length; j++) {
                        const relation = portfolio_relation_technologies.rows[j];
                        
                        await pool.query(`SELECT * FROM technologies where id = ${relation.technologie_id}`)
                        .then(technologies=>{
                            for (let k = 0; k < technologies.rows.length; k++) {
                                const technologie = technologies.rows[k];
                                portfolio.technologies.push(technologie)
                            }
                        }).catch(err3=>{
                            console.log('err3', err3);
                        })
                    }


                }).catch(err2=>{
                    console.log('err2', err2);
                })

                aux.push(portfolio)
        }


        response.status(200).json(aux)
    }).catch(err=>{
        response.status(400).json(err)
    })

});

app.post('/', (request, response) => {
    response.status(200).json('portfolio post World!')
});

app.put('/', (request, response) => {
    response.status(200).json('portfolio update World!')
});

app.delete('/', (request, response) => {
    response.status(200).json('portfolio delete World!')
});

module.exports = app
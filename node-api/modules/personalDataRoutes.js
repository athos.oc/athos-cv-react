const express = require('express')
const app = express()

const pool = require('./db')


app.get('/', (request, response) => {
    pool.query('SELECT * FROM personal_data')
    .then(res=>{
        const aux = {}
        for (let i = 0; i < res.rows.length; i++) {
            const row = res.rows[i];
            aux[row.item_name] = row.item_value
        }
        
        response.status(200).json(aux)
    }).catch(err=>{
        response.status(400).json(err)
    })


});

app.post('/', (request, response) => {
    response.status(200).json('personal_data personal_data post World!')
});

app.put('/', (request, response) => {
    response.status(200).json('personal_data personal_data update World!')
});

app.delete('/', (request, response) => {
    response.status(200).json('personal_data personal_data delete World!')
});

module.exports = app
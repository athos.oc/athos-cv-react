const express = require('express')
const app = express()
const pool = require('./db')
const _ = require('lodash');

app.get('/', (request, response) => {
    pool.query('SELECT * FROM profesional_experience')
    .then(async experiences=>{
        const aux = []

        for (let i = 0; i < experiences.rows.length; i++) {
            const experience = experiences.rows[i];
            experience.jobs=[]

            try {
                await pool.query(`SELECT * FROM jobs where profesional_experience_id=${experience.id}`)
                .then(jobs=>{
                    for (let j = 0; j < jobs.rows.length; j++) {
                        const job = jobs.rows[j];
                        experience.jobs.push(job.text)
                    }
                }).catch(err2=>{
                    console.log('err2', err2);
                })
                
            } catch (error) {
                console.log('error', error);
            }


            aux.push(experience)
        }


        response.status(200).json(aux)
    }).catch(err=>{
        response.status(400).json(err)
    })
});

app.post('/', (request, response) => {
    response.status(200).json('profesional_experience post World!')
});

app.put('/', (request, response) => {
    response.status(200).json('profesional_experience update World!')
});

app.delete('/', (request, response) => {
    response.status(200).json('profesional_experience delete World!')
});

module.exports = app
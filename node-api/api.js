
const express = require('express')
const app = express()
const port = 3001
var cors = require('cors')

app.use(cors())

app.use('/certifications', require('./modules/certificationsRoutes'))
app.use('/education', require('./modules/educationRoutes'))
app.use('/git_repos', require('./modules/gitReposRoutes'))
app.use('/personal_data', require('./modules/personalDataRoutes'))
app.use('/portfolio', require('./modules/portfolioRoutes'))
app.use('/profesional_experience', require('./modules/profesionalExperienceRoutes'))
app.use('/skills', require('./modules/skillsRoutes'))

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})


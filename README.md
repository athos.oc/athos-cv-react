# Requirements

Docker and docker-compose are requirements to run this project

# Note:

Tested on linux and osx

# Instructions

1.- Clone repo: 
   
```bash
git clone https://gitlab.com/athos.oc/athos-cv-react.git
```

2.- Enter on repo folder:

```bash
cd athos-cv-react
```

3.- start docker-compose:

```bash
docker-compose up
```

4.- Open this url on browser:

```bash
http://localhost:3000
```
   
# Todos

1. Improve responsive design form mobile
2. Db inserts, updates and deletes
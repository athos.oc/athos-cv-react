import React from 'react';
import HomeLayout from './layouts/HomeLayout';
import './App.scss';

function App() {
  return (
    <HomeLayout/>
  );
}

export default App;

import React, { useState, useEffect } from 'react';
import Header from '../components/Header'
import AboutMe from '../components/AboutMe'
import Contact from '../components/Contact'
import Education from '../components/Education'
import Skills from '../components/Skills'
import Certifications from '../components/Certifications'
import ProfesionalExperience from '../components/ProfesionalExperience'
import GitRepos from '../components/GitRepos'
import Portfolio from '../components/Portfolio'
import Footer from '../components/Footer'
import axios from 'axios';
import { API } from '../config'
import './HomeLayout.scss'

const HomeLayout = () => {
    const [personalData, setPersonalData] = useState([])
    const [education, setEducation] = useState([])
    const [skills, setSkills] = useState([])
    const [certifications, setCertifications] = useState([])
    const [profesionalExperience, setProfesionalExperience] = useState([])
    const [gitRepos, setGitRepos] = useState([])
    const [portfolio, setPortfolio] = useState([])

    useEffect(() => {
        try {
            axios.get(`${API}/personal_data`).then(res=>{setPersonalData(res.data)})
            axios.get(`${API}/education`).then(res=>{setEducation(res.data)})
            axios.get(`${API}/skills`).then(res=>{setSkills(res.data)})
            axios.get(`${API}/certifications`).then(res=>{setCertifications(res.data)})
            axios.get(`${API}/profesional_experience `).then(res=>{setProfesionalExperience(res.data)})
            axios.get(`${API}/git_repos `).then(res=>{setGitRepos(res.data)})
            axios.get(`${API}/portfolio `).then(res=>{setPortfolio(res.data)})
        } catch (error) {
            console.error("Error getting data: ", error)
        }
    },[]);

    return(
        <div className="grid-container home-layout-container">

                    <div className="grid-x">
                        <div className="cell medium-12 large-12"><Header personalData={personalData}/></div>
                    </div>
                    <div className="grid-x grid-margin-x top1rem">
                        <div className="cell medium-6 large-9"><AboutMe personalData={personalData}/></div>
                        <div className="cell medium-6 large-3"><Contact personalData={personalData}/></div>
                    </div>
                    <div className="grid-x knowledge top1rem">
                        <div className="cell medium-6 large-4"><Education education={education}/></div>
                        <div className="cell medium-6 large-4"><Skills skills={skills}/></div>
                        <div className="cell medium-6 large-4"><Certifications certifications={certifications}/></div>
                    </div>
                    <div className="grid-x top1rem">
                        <div className="cell medium-6 large-12"><ProfesionalExperience profesionalExperience={profesionalExperience}/></div>
                    </div>
                    <div className="grid-x top1rem">
                        <div className="cell medium-6 large-12"><GitRepos gitRepos={gitRepos}/></div>
                    </div>
                    <div className="grid-x top1rem">
                        <div className="cell medium-6 large-12"><Portfolio portfolio={portfolio}/></div>
                    </div>

                    <div className="grid-x top1rem">
                        <div className="cell medium-6 large-12"><Footer/></div>
                    </div>
        </div>
    )
}

export default HomeLayout;
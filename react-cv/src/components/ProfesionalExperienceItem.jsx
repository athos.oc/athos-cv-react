import React from 'react';
import './ProfesionalExperienceItem.scss';

const ProfesionalExperienceItem = (props) => {
    const {date_str, title, jobs} = props.item
    return (
        <div className="profesional-experience-item-container grid-x">
            <div className="cell medium-12 large-3">
                <div className="title-under">
                    {date_str}
                </div>
            </div>
            <div className="cell medium-12 large-9">
                <div className="title">{title}</div>
                <ul>
                    {
                        Array.isArray(jobs) && jobs.map(job=>{
                            return (<li key={job}>{job}</li>)
                        })
                    }
                </ul>
            </div>
            
        </div>
    );

};

export default ProfesionalExperienceItem
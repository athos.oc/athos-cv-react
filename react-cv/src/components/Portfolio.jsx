import React from 'react';
import './Portfolio.scss';
import PortfolioItem from './PortfolioItem';

const Portfolio = ({portfolio}) => {
  
    return(
        <div className="portfolio-container top1rem">
            <div className="title-under">
                public portfolio
            </div>
            {
                Array.isArray(portfolio) && portfolio.map(item=>{
                    return(
                        <PortfolioItem key={item.title} item={item}></PortfolioItem>        
                    )
                })
            }
        </div>
    )
}

export default Portfolio;
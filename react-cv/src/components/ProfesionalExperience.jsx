import React from 'react';
import './ProfesionalExperience.scss';
import ProfesionalExperienceItem from './ProfesionalExperienceItem';

const ProfesionalExperience = ({profesionalExperience}) => {
    return(
        <div className="profesional-experience-container">
            <div className="title-under">profesional experience</div>
            {
                Array.isArray(profesionalExperience) && profesionalExperience.map(item=>{
                    return(
                        <ProfesionalExperienceItem key={item.id} item={item}/>
                    )
                })
            }
        </div>
    )
}

export default ProfesionalExperience;
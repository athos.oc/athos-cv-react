import React from 'react';
import './Skills.scss';
import TitleWithBackground from './TitleWithBackground'

const Skills = ({skills}) => {
  
    return(
        <div className="skill-container">
            <div className="knowledge-title">
                <TitleWithBackground 
                    text={"Skills"}
                    textColor={"#FFFFFF"}
                    bgColor={"#595959"}
                />
            </div>
            {
                Array.isArray(skills) && skills.map(item=>{
                    return(
                        <div key={item.title} className="skill-content">{item.title}</div>        
                    )
                })
            }
        </div>
    )
}

export default Skills;
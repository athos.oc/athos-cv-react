import React from 'react';
import './Education.scss';
import TitleWithBackground from './TitleWithBackground'
const Education = ({education}) => {
  
    return(
        <div className="education-container">
            <div className="knowledge-title">
                <TitleWithBackground 
                    text={"Education"}
                    textColor={"#FFFFFF"}
                    bgColor={"#A5B592"}
                />
            </div>
            {
                Array.isArray(education) && education.map(item=>{
                    return(
                        <div className="education-content-item" key={item.title}>
                            <div className="title">{item.title}</div>
                            <div className="place">{item.place}</div>
                        </div>        
                    )
                })
            }
        </div>
    )
}

export default Education;
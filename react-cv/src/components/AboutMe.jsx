import React from 'react';
import './AboutMe.scss';

const AboutMe = ({personalData}) => {
  
    return(
        <div className="aboutme-container">
            <div className="title-under">about me</div>
            <div>{personalData.about_me}</div>
        </div>
    )
}

export default AboutMe;
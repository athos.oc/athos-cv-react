import React from 'react';
import './PortfolioItem.scss';

const PortfolioItem = ({item}) => {
    const {title, url, technologies, description } = item;
    console.log(technologies);
    return(
        <div className="portfolio-item-container grid-x top1rem">
            <div className="cell medium-12 large-3">
                <div className="title-under">
                    {title}
                </div>
            </div>
            <div className="cell medium-12 large-9 items">
                <div className="title-under">Link:</div>
                <div className="top1rem"><a href={url}>{url}</a></div>
                <div>
                    <div>
                        <span className="title-under top1rem">Used technologies:</span>
                        <div>
                            {
                                Array.isArray(technologies) && technologies.map(tech=>{
                                    return (
                                        <span className="tech-name" key={tech.title}>{tech.title}, </span>
                                    )
                                })
                            }
                        </div>
                    </div>
                    
                    <div className="imgs top1rem">    
                        {
                            Array.isArray(technologies) && technologies.map(tech=>{
                                return (
                                    <img key={tech.title} src={"assets/imgs/"+tech.logo_img} alt=""/>
                                )
                            })
                        }
                    </div>
                </div>
                <div className="title-under top1rem">Description:</div>
                <div>{description}</div>

            </div>
        </div>
    );
};

export default PortfolioItem;
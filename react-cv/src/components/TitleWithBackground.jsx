import React from 'react';

const TitleWithBackground = ({text, textColor, bgColor}) => (
    <div style={{
        color:textColor,
        backgroundColor: bgColor,
        fontWeight:'bold',
        padding:'0 30%'
    }}>{text}</div>
);

export default TitleWithBackground
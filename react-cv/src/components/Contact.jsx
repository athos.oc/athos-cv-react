import React from 'react';
import './Contact.scss';

const Contact = ({personalData}) => {
  
    return(
        <div className="contact-container">
            <div className="title-under">Contact</div>
            <div className="contact-item"> 
                <div><img src="assets/imgs/phone-icon.png" alt=""/></div>
                <div className="content">{personalData.phone}</div>
            </div>
            <div className="contact-item">
                <div><img src="assets/imgs/email-icon.png" alt=""/></div>
                <div className="content">{personalData.email}</div>
            </div>
            <div className="contact-item">
                <div><img src="assets/imgs/location-icon.png" alt=""/></div>
                <div className="content">{personalData.city}</div>
            </div>
            <div className="contact-item">
                <div><img src="assets/imgs/linkedin-icon.png" alt=""/></div>
                <div className="content"><a href={personalData.linkedin}>linkedin/athos54</a></div>
            </div>
            <div className="contact-item">
                <div><img src="assets/imgs/world-icon.png" alt=""/></div>
                <div className="content">{personalData.web}</div>
            </div>
        </div>
    )
}

export default Contact;
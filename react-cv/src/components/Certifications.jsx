import React from 'react';
import './Certifications.scss';
import TitleWithBackground from './TitleWithBackground'

const Certifications = ({certifications}) => {
  
    return(
        <div className="certifications-container">
            <div className="knowledge-title">
                <TitleWithBackground 
                    text={"Certifications"}
                    textColor={"#FFFFFF"}
                    bgColor={"#A5B592"}
                />
            </div>
            {
                certifications.map(item=>{
                    return(
                        <div className="certifications-content-item" key={item.title}>
                            <div className="certifications-title">{item.title}</div>
                            <div>{item.place}</div>
                        </div>        
                    )
                })
            }
        </div>
    )
}

export default Certifications;
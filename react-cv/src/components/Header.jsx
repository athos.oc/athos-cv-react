import React from 'react';
import './Header.scss';

const Header = ({personalData}) => {

    return(
        <div className="header-container">
            <div className="title">INFORMATION TECHNOLOGY (IT)</div>
            <div className="title">CURRICULUM VITAE</div>
            <div className="title name">from {personalData.name} {personalData.lastname}</div>
        </div>
    )
}

export default Header;
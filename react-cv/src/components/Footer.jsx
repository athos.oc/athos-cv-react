import React from 'react';
import './Footer.scss';

const Footer = () => {
  
    return(
        <div className="footer-container">
            <div className="title-under">repositorios git</div>
           
            <div className="grid-x repo">
                <div className="cell medium-12 large-3">
                    <div className="title-under">
                        Repo
                    </div>
                </div>
                <div className="cell medium-12 large-9">
                    <div>The code repo is public and you can check the code on </div>
                    <div><a href="https://gitlab.com/athos.oc/athos-cv-react">https://gitlab.com/athos.oc/athos-cv-react</a></div>
                </div>
            </div>      

            <div className="grid-x repo">
                <div className="cell medium-12 large-3">
                    <div className="title-under">
                        Last updated
                    </div>
                </div>
                <div className="cell medium-12 large-9">
                    <div>Last update of this cv was on 2020/08/02</div>
                </div>
            </div>        
        </div>
    )
}

export default Footer;
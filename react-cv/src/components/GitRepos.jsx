import React from 'react';
import './GitRepos.scss';

const GitRepos = ({gitRepos}) => {
  
    return(
        <div className="git-repos-container">
            <div className="title-under">repositorios git</div>

            {
                Array.isArray(gitRepos) && gitRepos.map(item=>{
                    return(
                        <div key={item.title} className="grid-x repo">
                            <div className="cell medium-12 large-3">
                                <div className="title-under">
                                    {item.title}
                                </div>
                            </div>
                            <div className="cell medium-12 large-9"><a href={item.url}>{item.url}</a></div>
                        </div>        
                    )
                })
            }
        </div>
    )
}

export default GitRepos;
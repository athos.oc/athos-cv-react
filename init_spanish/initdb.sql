CREATE TABLE personal_data (
	id serial PRIMARY KEY,
	item_name VARCHAR (255) NOT NULL,
	item_value TEXT UNIQUE NOT NULL,
	sequence_number INTEGER NOT NULL
);

INSERT INTO personal_data (item_name, item_value, sequence_number) VALUES
 ('name', 'athos', 10),
 ('lastname', 'orio choperena', 20),
 ('about_me', 'Soy una persona que estoy continuamente desarrollando mis habilidades. Me gusta tanto la programación como la administración de sistemas. Creo que ambas habilidades juntas hacen una buena sinergia. Con el tiempo he desarrollado habilidades de fullstack (front y back) y del mundo devops. Tengo experiencia en el desarrollo de aplicaciones web, de escritorio y móviles, normalmente las construyo desde cero. No tengo miedo de aprender nuevas tecnologías, de hecho, me gusta el reto.', 30),
 ('phone', '(+34) 623-187-588', 40),
 ('email', 'athos.oc@gmail.com', 50),
 ('city', 'Logroño (ESPAÑA)', 60),
 ('linkedin', 'https://www.linkedin.com/in/athos-or%C3%ADo-choperena-a68201aa/', 70),
 ('web', 'https://athosnetwork.es/', 80);

 CREATE TABLE education (
     id serial PRIMARY KEY,
     title VARCHAR(255) NOT NULL,
     place VARCHAR(255) NOT NULL
 );

 INSERT INTO education (title, place) VALUES
 ('Técnico electrónico', 'Cosme García Inventor 2003'),
 ('Técnico en micro-ordenadores', 'Gobierno de la Rioja 2005'),
 ('Técnico superior en administración de sistemas', 'Cosme García Inventor 2003'),
 ('Técnico superior en desarrollo de aplicaciones web', 'IES Comercio 2018');

 CREATE TABLE skills (
     id serial PRIMARY KEY,
     title VARCHAR(255) NOT NULL,
     description TEXT
 );

 INSERT INTO skills (title, description) VALUES
 ('javascript', 'He realizado varios proyectos utilizando javascript. Es un lenguaje flexible y me gusta mucho. La asincronia que ofrece javascript es potente y puede llegar a ser caótica si no entiendes bien el funcionamiento.'),
 ('typescript', 'Typescript viene para controlar un poco ese lado caótico que tiene javascript, la posibilidad de \"tipar\" ayuda a controlar y tener menos problemas en tiempo de ejecución, y hace que el debug sea mas sencillo.'),
 ('angular', 'Angular es un framework que me encanta. He realizado tanto webs en diferentes versiones del framework, como aplicaciones móviles con ionic (que usa angular por debajo)'),
 ('react', 'React es una biblioteca que me gusta por su sencillez. Componetizar componentes y reutilizarlos es algo que ayuda mucho a la eficiencia programando. He realizado varios proyectos utilizando react. El front de donbackup (en desarrollo) esta realizado con react y dondns.es/ddns tambien.'),
 ('nodejs', 'Lo que más me gusta de nodejs es que puedes utilizar todos los conocimientos de javascript que sepas en el lado del backend. Atacar a apis hechas en nodejs y manipular los datos en la api es algo realmente sencillo y comodo. He realizado apis encapsuladas en microservicios en nodejs como por ejemplo para redimensionar imagenes, atacar a bases de datos sql o no-sql, scripts de importación etc.'),
 ('golang', 'Es un lenguaje sencillo y rápido. He realizado varias apis y software con golang con un rendimiento espectacular. Poder compilar el binario y ejecutarlo sin tener que instalar nada en el equipo destino es genial.'),
 ('firebase', 'Firebase es un baas que te ayuda mucho en varios aspectos, como la gestion de autenticación, publicacion de webs, base de datos y storage. Una de las cosas que mas me gusta de firebase es el sistema de subscripcion a colecciones, esto proporciona una reactividad increible a las aplicaciones sin tener que escribir mucho codigo'),
 ('mysql', ''),
 ('postgresql', ''),
 ('mongo', ''),
 ('docker', 'No se que decir de docker a parte de que no podría vivir sin el. El echo de que puedas crear tus imagenes a gusto, y reutilizarlas, sin tener que instalar cosas en tu equipo es increible. Aumenta la productividad muchisimo'),
 ('lxc', 'Lxc es un sistema de "virtualizacion" que puede resultar muy util para montar infraestructuras, el sistema de snapshot y poder mover estos entre servidores es increible'),
 ('administración de sistemas', ''),
 ('rabbitmq', '');

 CREATE TABLE certifications(
     id serial PRIMARY KEY,
     title VARCHAR(255) NOT NULL,
     description TEXT
 );

INSERT INTO certifications (title, description) VALUES
 ('Unity 3d Diploma', ''),
 ('Linux Essentials LPI', ''),
 ('Técnico electrónico', ''),
 ('Técnico superior en administración de sistemas', '');

 CREATE TABLE profesional_experience(
     id serial PRIMARY KEY,
     date_str VARCHAR(255),
     title VARCHAR(255)
 );

 INSERT INTO profesional_experience(date_str, title) VALUES
 ('2016-presente', 'Desarrollador fullstack && Administración de sistemas'),
 ('2016-2016', 'Formador técnico. Senda Formación'),
 ('2016-2016', 'Técnico informático. Magic Systems'),
 ('2015-2015', 'Formador técnico. Senda Formación'),
 ('2007-2016', 'Negocio propio. Servicio técnico. Veroos Solutions'),
 ('2005-2007', 'Servicio técnico. Imei telecom');


 CREATE TABLE jobs(
     id serial PRIMARY KEY,
     text VARCHAR(255) NOT NULL,
     profesional_experience_id INTEGER,
     CONSTRAINT fk_profesional_experience
      FOREIGN KEY(profesional_experience_id) 
	  REFERENCES profesional_experience(id)
 );

INSERT INTO jobs(text, profesional_experience_id) VALUES
('Maquetar frontend', 1),
('Desarrollador aplicaciones móvil hibridas (para android y ios)', 1),
('Backends personalizados para clientes', 1),
('Creación y mantenimiento de insfraestructura', 1),
('Requisitar necesidades del cliente y su futuro software', 1),
('Transmitir conocimientos a otros e introducirlos a varias tecnologías', 2),
('Reparación de ordenadores', 3),
('Despliegue automático de sistemas operativos salvando semanas de tiempo a la empresa', 3),
('Investigación y desarrollo', 3),
('Transmitir conocimientos a otros e introducirlos a varias tecnologías', 4),
('Coordinación de personal', 5),
('Compras', 5),
('Cumplir con las necesidades de los clientes', 5),
('Reparación de ordenadores', 5),
('Reparación de móviles', 5),
('Creación de software de gestion de la empresa', 5),
('Desarrollo web', 5),
('Servicios de hosting', 5),
('Asesoramiento técnico', 5),
('Cumplir con las necesidades de los clientes', 6),
('Reparación de ordenadores', 6),
('Reparación de móviles', 6),
('Creación de software de gestion de la empresa', 6);

CREATE TABLE git_repos (
    id serial PRIMARY KEY,
    title VARCHAR(255),
    url VARCHAR(255)
);

INSERT INTO git_repos(title, url) VALUES
('Github', 'https://github.com/athos54'),
('Gitlab', 'https://gitlab.com/athos.oc');

CREATE TABLE technologies(
    id serial PRIMARY KEY,
    title VARCHAR(255) UNIQUE,
    logo_img VARCHAR(255)
);

INSERT INTO technologies(title, logo_img) VALUES
('GO/GOLANG','golang-logo.png'),
('ELECTRON','electron-logo.png'),
('JAVASCRIPT','javascript-logo.png'),
('REACT','react-logo.png'),
('RABBITMQ','rabbitmq-logo.png'),
('POSTGRESS','postgresql-logo.png'),
('COUCHDB','couchdb-logo.png'),
('DOCKER','docker-logo.png'),
('IONIC','ionic-logo.png'),
('ANGULAR','angular-logo.png'),
('TYPESCRYPT','typescript-logo.png'),
('FIREBASE','firebase-logo.png'),
('NODEJS','nodejs-logo.png'),
('HANDLEBARS','handlebars-logo.png');

CREATE TABLE portfolio(
    id serial PRIMARY KEY,
    title VARCHAR(255),
    url VARCHAR(255),
    description text
);

INSERT INTO portfolio(title, url, description) VALUES
('Donbackup', 'https://donbackup.com', 'En desarrollo. Software de última generación para gestionar, programar tareas de copias de seguridad en multiples servidores y ordenadores de escritorio a través de un interface desacoplado. Es multi-plataforma y usa tecnologías de mensajería como Rabbitmq'),
('Servicio DDNS', 'https://dondns.es/ddns/', 'Servicio gratuito de dns dinámico con webhooks integrados'),
('SportTavern ANDROID/IOS', 'https://play.google.com/store/apps/details?id=com.proyecto.online.sporttavernapp&gl=ES', 'Aplicación de reparto de comida a domicilio con geolocalización'),
('iTransporte ANDROID/IOS', 'https://apps.apple.com/es/app/sportavern/id1337908939', 'Herramientas para transportistas con gestion de incidencias de tráfico, tiempo y congestión de tráfico.'),
('El Búho Lotero ANDROID/IOS', 'https://play.google.com/store/apps/details?id=com.proyecto.online.loteriaselbuho', 'Aplicación de sorteos y apuestas');


CREATE TABLE portfolio_technologies(
    id serial PRIMARY KEY,
    portfolio_id INTEGER,
    technologie_id INTEGER
);

INSERT INTO portfolio_technologies(portfolio_id, technologie_id) VALUES
(1,1),(1,2),(1,3),(1,4),(1,5),(1,6),
(2,1),(2,3),(2,4),(2,7),(2,8),
(3,9),(3,10),(3,2),(3,3),(3,11),(3,12),(3,13),(3,14),
(4,9),(4,10),(4,3),(4,11),(4,12),(4,13),
(5,9),(5,10),(5,3),(5,11),(5,12),(5,13);
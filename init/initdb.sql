CREATE TABLE personal_data (
	id serial PRIMARY KEY,
	item_name VARCHAR (255) NOT NULL,
	item_value TEXT UNIQUE NOT NULL,
	sequence_number INTEGER NOT NULL
);

INSERT INTO personal_data (item_name, item_value, sequence_number) VALUES
 ('name', 'athos', 10),
 ('lastname', 'orio choperena', 20),
 ('about_me', ' Im Spanish and I am continuously developing my skills. I enjoy both programming and systems administration. I believe both skills together make a good synergy. I have knowledge of full stack development (front and back) and devops world. I have experience in developing web, desktop and mobile applications from scratch. Im not afraid to learn new technologies, indeed I welcome the challenge.', 30),
 ('phone', '(+34) 623-187-588', 40),
 ('email', 'athos.oc@gmail.com', 50),
 ('city', 'Logroño (ESPAÑA)', 60),
 ('linkedin', 'https://www.linkedin.com/in/athos-or%C3%ADo-choperena-a68201aa/', 70),
 ('web', 'https://athosnetwork.es/', 80);

 CREATE TABLE education (
     id serial PRIMARY KEY,
     title VARCHAR(255) NOT NULL,
     place VARCHAR(255) NOT NULL
 );

 INSERT INTO education (title, place) VALUES
 ('Electronic Technician', 'Cosme García Inventor 2003'),
 ('Micro-computers technician', 'Spain Goverment 2005'),
 ('System administrator - High technician', 'IES Comercio 2018'),
 ('Web developer - High technician', 'IES Comercio 2020');

 CREATE TABLE skills (
     id serial PRIMARY KEY,
     title VARCHAR(255) NOT NULL,
     description TEXT
 );

 INSERT INTO skills (title, description) VALUES
 ('javascript', ''),
 ('typescript', ''),
 ('angular', ''),
 ('react', ''),
 ('nodejs', ''),
 ('golang', ''),
 ('firebase', ''),
 ('mysql', ''),
 ('postgresql', ''),
 ('mongo', ''),
 ('docker', ''),
 ('lxc', ''),
 ('system administrator', ''),
 ('rabbitmq', '');

 CREATE TABLE certifications(
     id serial PRIMARY KEY,
     title VARCHAR(255) NOT NULL,
     description TEXT
 );

INSERT INTO certifications (title, description) VALUES
 ('Unity 3d Diploma', ''),
 ('Linux Essentials LPI', ''),
 ('Electronic Technician', ''),
 ('System administrator - High technician', '');

 CREATE TABLE profesional_experience(
     id serial PRIMARY KEY,
     date_str VARCHAR(255),
     title VARCHAR(255)
 );

 INSERT INTO profesional_experience(date_str, title) VALUES
 ('2016-now', 'DEVOPS && Fullstack Developer && Server administration'),
 ('2016-2016', 'Technical trainer. Senda Formación'),
 ('2016-2016', 'Computer technical. Magic Systems'),
 ('2015-2015', 'Technical trainer. Senda Formación'),
 ('2007-2016', 'Self bussines. Technical service. Veroos Solutions'),
 ('2005-2007', 'Technical service. Imei telecom');


 CREATE TABLE jobs(
     id serial PRIMARY KEY,
     text VARCHAR(255) NOT NULL,
     profesional_experience_id INTEGER,
     CONSTRAINT fk_profesional_experience
      FOREIGN KEY(profesional_experience_id) 
	  REFERENCES profesional_experience(id)
 );

INSERT INTO jobs(text, profesional_experience_id) VALUES
('Fullstack Developer && Server administration', 1),
('Hybrid Mobile developer (to android and ios)', 1),
('Build custom backends for clients with golang', 1),
('Build custom backends for clients with nodejs', 1),
('Build and maintenance apps insfrastructure', 1),
('Customer interview to know their neededs', 1),
('Firebase functions maintenance (nodejs)', 1),
('Transmitting knowledge to others and introducing them to various technologies', 2),
('Computers repair', 3),
('Multideploy operating system saving weeks of time to organization', 3),
('Research and development', 3),
('Transmitting knowledge to others and introducing them to various technologies', 4),
('Manage people', 5),
('Do buys', 5),
('Meet customers demands', 5),
('Computers repair', 5),
('Mobile phone repair', 5),
('Build organization software manager', 5),
('Web development', 5),
('Hosting services', 5),
('Technical advice.', 5),
('Meet customers demands', 6),
('Computers repair', 6),
('Mobile phone repair', 6),
('Build organization software manager', 6);

CREATE TABLE git_repos (
    id serial PRIMARY KEY,
    title VARCHAR(255),
    url VARCHAR(255)
);

INSERT INTO git_repos(title, url) VALUES
('Github', 'https://github.com/athos54'),
('Gitlab', 'https://gitlab.com/athos.oc');

CREATE TABLE technologies(
    id serial PRIMARY KEY,
    title VARCHAR(255) UNIQUE,
    logo_img VARCHAR(255)
);

INSERT INTO technologies(title, logo_img) VALUES
('GO/GOLANG','golang-logo.png'),
('ELECTRON','electron-logo.png'),
('JAVASCRIPT','javascript-logo.png'),
('REACT','react-logo.png'),
('RABBITMQ','rabbitmq-logo.png'),
('POSTGRESS','postgresql-logo.png'),
('COUCHDB','couchdb-logo.png'),
('DOCKER','docker-logo.png'),
('IONIC','ionic-logo.png'),
('ANGULAR','angular-logo.png'),
('TYPESCRYPT','typescript-logo.png'),
('FIREBASE','firebase-logo.png'),
('NODEJS','nodejs-logo.png'),
('HANDLEBARS','handlebars-logo.png');

CREATE TABLE portfolio(
    id serial PRIMARY KEY,
    title VARCHAR(255),
    url VARCHAR(255),
    description text
);

INSERT INTO portfolio(title, url, description) VALUES
('Donbackup', 'https://donbackup.com', 'On development. Sofisticated software to manage backups, programmed backup tasks on multiple servers and desktops by one decoupled interface. It is multi-platform and use awesome technologies like Rabbitmq'),
('DDNS servicio', 'https://dondns.es/ddns/', 'Free dinamic dns service with integrated webhooks'),
('SportTavern ANDROID/IOS', 'https://play.google.com/store/apps/details?id=com.proyecto.online.sporttavernapp&gl=ES', 'Geolocation home-delivery application'),
('iTransporte ANDROID/IOS', 'https://apps.apple.com/es/app/sportavern/id1337908939', 'Tools for transporters with management of traffic incidents, weather and traffic congestion'),
('El Búho Lotero ANDROID/IOS', 'https://play.google.com/store/apps/details?id=com.proyecto.online.loteriaselbuho', 'Tools for transporters with management of traffic incidents, weather and traffic congestion');


CREATE TABLE portfolio_technologies(
    id serial PRIMARY KEY,
    portfolio_id INTEGER,
    technologie_id INTEGER
);

INSERT INTO portfolio_technologies(portfolio_id, technologie_id) VALUES
(1,1),(1,2),(1,3),(1,4),(1,5),(1,6),
(2,1),(2,3),(2,4),(2,7),(2,8),
(3,9),(3,10),(3,2),(3,3),(3,11),(3,12),(3,13),(3,14),
(4,9),(4,10),(4,3),(4,11),(4,12),(4,13),
(5,9),(5,10),(5,3),(5,11),(5,12),(5,13);